import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AboutMeComponent } from './about-me.component';

const routes: Routes = [
  { path: '', component: AboutMeComponent}
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AboutMeRoutingModule {}
