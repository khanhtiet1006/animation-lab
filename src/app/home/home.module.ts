import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { TextMoveComponent } from './text-move/text-move.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [HomeComponent, TextMoveComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class HomeModule { }
