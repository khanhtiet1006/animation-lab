import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './articles/article.component';
import { ArticleRoutingModule } from './article-routing.module';
import { ArticleDetailsComponent } from './article-details/article-details.component';
import { PrismModule } from '@ngx-prism/core';

@NgModule({
  declarations: [
    ArticleComponent,
    ArticleDetailsComponent
  ],
  imports: [
    CommonModule,
    ArticleRoutingModule,
    PrismModule
  ]
})
export class ArticleModule { }
